(function( g ) {

	// Definitions
	this.household = document.querySelector( 'ol.household' );
	this.ageField = document.querySelector( 'input[name=age]' );
	this.relationField = document.querySelector( 'select[name=rel]' );
	this.smokerField = document.querySelector( 'input[name=smoker]' );
	this.addBtn = document.querySelector( 'button.add' );
	this.submitBtn = document.querySelector( 'button[type=submit]' );
	this.debugArea = document.querySelector( 'pre.debug' );


	// Adds a member to the list
	this.addMember = function( e ) {
		e.preventDefault();

		var age = this.ageField.value,
			relationship = this.relationField.value,
			smoker = this.smokerField.checked;

		// Check age field
		if( isNaN( age ) || age < 1 ) {
			alert( 'Age must be a number that is greater than 0.' );
			return false;
		}

		// Check relationship field
		if( !relationship.length ) {
			alert( 'Relationship is required!' );
			return false;
		}

		this.createMemberField({
			age: age,
			relationship: relationship,
			smoker: smoker
		});
		this.resetFields();
	};

	// Removes member from the household
	this.removeMember = function( e ) {
		var node =  e.target.parentNode;

		var c = confirm( 'Are you sure you want to remove ' + node.memberInfo.relationship + '?' );
		if( c ) {
			node.parentNode.removeChild( node );
		}
	};

	// Adds the actual element to the page
	this.createMemberField = function( data ) {
		var el = document.createElement( 'li' );
		el.innerHTML = '<b>' + this.relationField.querySelector( '[value=' + data.relationship + ']' ).text + '</b> - ' + data.age + ' - <i>' + ( data.smoker ? 'Smoker' : 'Non-Smoker' ) + '</i> <button name="remove">x</button>';
		el.memberInfo = data;

		el.querySelector( 'button[name=remove]' ).addEventListener( 'click', function( e ) { this.removeMember( e ); }.bind( this ) );
		this.household.appendChild( el );
	};

	// Send data
	this.submitMembers = function( e ) {
		e.preventDefault();
		if( this.ageField.value.length && this.relationField.value.length ) {
			this.addMember( e );
		}
		this.resetFields();

		var nodes = this.household.childNodes, i = 0, len = nodes.length, out = { household: [] };
		while( i < len ) {
			out.household.push( nodes[ i ].memberInfo );
			i++;
		}

		if( out.household.length ) {
			this.debugArea.innerText = JSON.stringify( out );
			this.debugArea.style.display = 'block';
			this.debugArea.style.whiteSpace = 'normal';
		} else {
			this.debugArea.style.display = 'none';
		}
	};

	// Kill the values!
	this.resetFields = function() {
		this.ageField.value = '';
		this.relationField.value = '';
		this.smokerField.checked = false;
	};


	// Setup Listeners
	this.addBtn.addEventListener( 'click', function( e ) { this.addMember( e ); }.bind( this ) );
	this.submitBtn.addEventListener( 'click', function( e ) { this.submitMembers( e ); }.bind( this ) );
})( window );
